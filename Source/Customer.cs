using Godot;
using System;

public partial class Customer : Area2D
{	
	public Action<FoodItem> OnWalkEnded;
	
	[Export] public FoodItem DesiredFood = FoodItem.Cupcake;
	[Export] public float Speed;
	
	private AnimatedSprite2D animator;
	private bool isWalking = false;
	private Marker2D goal;
	private float upperBoundaryX;
	
	public override void _Ready()
	{
		animator = GetNode<AnimatedSprite2D>("Animator");
		animator.Play(name: "idle");
		upperBoundaryX = GetViewportRect().Size.X;
	}
	
	public override void _Process(double delta)
	{
		if(isWalking)
		{
			float x = Mathf.Clamp(Position.X - ((float)delta * Speed), goal.Position.X, upperBoundaryX);
			Position = new Vector2(x, Position.Y);
			if(x == goal.Position.X)
			{
				Idle();
				OnWalkEnded?.Invoke(DesiredFood);
			}
		}
	}
	
	public void WalkTowards(Marker2D goal)
	{
		isWalking = true;
		this.goal = goal;
		animator.Play(name: "walk");
	}
	
	public void Idle()
	{
		isWalking = false;
		animator.Play(name: "idle");
	}

	public void RequestFood(Texture2D foodTexture)
	{
		Sprite2D speechBubble = GetNode<Sprite2D>("Speech Bubble");
		Sprite2D foodSprite = speechBubble.GetNode<Sprite2D>("Food");
		foodSprite.Texture = foodTexture;
		speechBubble.Visible = true;
	}
}
