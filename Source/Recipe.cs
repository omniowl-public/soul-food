using System;
using System.Collections.Generic;

public class Recipe
{
	public List<IngredientType> Ingredients;
	public FoodItem Result;
	
	public Recipe(IngredientType first, IngredientType second, IngredientType third)
	{
		Ingredients = new();
		Ingredients.Add(first);
		Ingredients.Add(second);
		Ingredients.Add(third);
	}
}
