using Godot;
using System;

public partial class StartScreen : CanvasLayer
{
	[Signal]
	public delegate void StartGameEventHandler();
	
	[Export] public Button StartButton { get; set; }
	[Export] public Label PressStartLabel { get; set; }
	[Export] public Label CreditLabel { get; set; }
	[Export] public float PressStartLabelAnimTime { get; set; } = 1f;
	[Export] public ColorRect WhiteOutRect { get; set; }
	[Export] public float WhiteOutAnimTime { get; set;} = 2f;
	
	private Tween pressStartLabelTween;
	private Color pressStartLabelColor;
	private Color pressStartLabelTransparent;
	private float pingpongTime = 0f;
	private bool isRunningPingPong = false;
	private bool isPingPongReversed = false;
	
	private Color white;
	private Color whiteTransparent;
	private float whiteoutTime;
	private bool isRunningWhiteOut;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		CreditLabel.Show();
		PressStartLabel.Hide();
		pressStartLabelColor = PressStartLabel.GetThemeColor("font_color");
		pressStartLabelTransparent 
			= new Color(pressStartLabelColor.R, pressStartLabelColor.G, pressStartLabelColor.B, 0f);
		StartButton.Hide();
		WhiteOutRect.Hide();
		SetPressStartLabelText();
		
		white = new Color(1f, 1f, 1f, 1f);
		whiteTransparent = new Color(1f, 1f, 1f, 0f);
	}
	
	private void SetPressStartLabelText()
	{
		switch (OS.GetName())
		{
			case "Android":
			case "iOS":
				GD.Print("Android/iOS Agent. Mobile Assumed");
				PressStartLabel.Text = "Touch Anywhere To Apply";
			break;
			case "Windows":
			case "UWP":
			case "macOS":
			case "Linux":
			case "FreeBSD":
			case "NetBSD":
			case "OpenBSD":
			case "BSD":
			case "Web":
			default:
				GD.Print("PC/Mac/UWP/Linux/Web or Default. Computer Assumed");
				PressStartLabel.Text = "Press Any Button To Apply";
			break;
		}
	}
	
	private void OnStartTimerTimeout()
	{
		PressStartLabel.AddThemeColorOverride("font_color", pressStartLabelTransparent);
		PressStartLabel.Show();
		StartButton.Show();
		isRunningPingPong = true;
	}
	
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if(isRunningPingPong) StartTimerLabelPingPong(delta);
		if(isRunningWhiteOut) WhiteOutAnim(delta);
	}
	
	private void OnStartButtonPressed()
	{
		StartButton.Hide();
		WhiteOutRect.Show();
		isRunningWhiteOut = true;
		isRunningPingPong = false;
	}
	
	public override void _UnhandledInput(InputEvent @event)
	{
		if(isRunningPingPong && @event is InputEventKey eventKey)
			if (eventKey.Pressed) OnStartButtonPressed();
	}
	
#region Animation
	private void StartTimerLabelPingPong(double delta)
	{
		Color start , end;
		if(pingpongTime >= PressStartLabelAnimTime)
		{
			pingpongTime = 0f;
			isPingPongReversed = !isPingPongReversed;
		}
		
		if(isPingPongReversed)
		{
			start = pressStartLabelTransparent;
			end = pressStartLabelColor;
		}
		else
		{
			start = pressStartLabelColor;
			end = pressStartLabelTransparent;
		}
		
		pingpongTime += (float)delta;
		
		float alpha = pingpongTime / PressStartLabelAnimTime;
		Color newColor = start.Lerp(end, alpha);
		PressStartLabel.AddThemeColorOverride("font_color", newColor);
	}
	
	private void WhiteOutAnim(double delta)
	{
		whiteoutTime += (float)delta;
		
		float alpha = whiteoutTime / WhiteOutAnimTime;
		WhiteOutRect.Color = whiteTransparent.Lerp(white, alpha);
		if(whiteoutTime >= WhiteOutAnimTime)
		{
			isRunningWhiteOut = false;
			EmitSignal(SignalName.StartGame);
			QueueFree();
		}
	}
#endregion
}
