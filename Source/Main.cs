using Godot;
using System;
using System.Collections.Generic;

public partial class Main : Node
{
	[Signal] public delegate void WhiteOutFinishedEventHandler();
	
	[Export] public StartScreen @StartScreen { get; set; }
	[Export] public ColorRect WhiteOutRect { get; set; }
	[Export] public float WhiteOutAnimTime { get; set; } = 1f;
	[Export] public Player @Player { get; set; }
	[Export] public AudioStreamPlayer Audio { get; set; }
	[Export] public Timer GodTimer { get; set; }
	[Export] public Texture2D[] FoodTextures { get; set; }

	// Player
	public bool CanMove { get; private set; } = false;
	public bool CanInteract { get; private set; } = false;
	
	private bool canUseDesk = false;
	
	// Whiteout
	private bool isDoingWhiteOut;
	private float whiteoutTime = 0f;
	private bool isWhiteOut = true;
	private Color whiteColor = new Color(1f, 1f, 1f, 1f);
	private Color whiteColorTransparent = new Color(1f, 1f, 1f, 0f);
	private Action WhiteOutCallback = null;
	
	private List<IngredientType> typesHeld = new List<IngredientType>();
	private List<Recipe> recipes = new List<Recipe>();
	
	private Marker2D cafeteriaDeskMarker;
	
	public override void _Ready()
	{
		@StartScreen.Show();
		@Player.Hide();
		WhiteOutRect.Hide();
		GetNode<God>("God").OnDialogueFinished += OnGodDialogueFinished;
		
		GetNode<Ingredient>("Ingredient").OnPickedIngredient += OnPickedIngredient;
		GetNode<Ingredient>("Ingredient2").OnPickedIngredient += OnPickedIngredient;
		GetNode<Ingredient>("Ingredient3").OnPickedIngredient += OnPickedIngredient;
		recipes.Add(new Recipe(IngredientType.Pink, IngredientType.Blue, IngredientType.Purple));
		cafeteriaDeskMarker = GetNode("Cafeteria").GetNode<Marker2D>("RightBoundary");
	}
	
	private void OnStartScreenStartGame()
	{
		// Game Starts from Here
		GD.Print("Game Start");
		isWhiteOut = false;
		isDoingWhiteOut = true;
		StartWhiteOutAnim(isWhiteOut: false, isDoingWhiteOut: true, callback: (() => 
		{
			Audio.Play();
			@Player.Show();
			WhiteOutRect.Hide();
			GodTimer.Start();
		}));
	}
	
	private void OnGodTimerTimeout()
	{
		Audio.Play();
		CanInteract = true;
		God god = GetNode<God>("God");
		god.Show();
		god.EnableGod();
	}
	
	private void OnGodDialogueFinished()
	{
		CanMove = true;

		Customer customer = GetNode<Customer>("Elemental");
		customer.WalkTowards(GetNode<Marker2D>("CustomerMarker"));
		customer.OnWalkEnded += OnCustomerArrived;
	}
	
	private void StartWhiteOutAnim(bool isWhiteOut, bool isDoingWhiteOut, Action callback = null)
	{
		if(callback != null) WhiteOutCallback = callback;
		
		this.isWhiteOut = isWhiteOut;
		this.isDoingWhiteOut = isDoingWhiteOut;
		WhiteOutRect.Show();
	}
	
	private void OnWhiteOutFinished()
	{
		if(WhiteOutCallback != null)
		{
			WhiteOutCallback.Invoke();
			WhiteOutCallback = null;
			WhiteOutRect.Hide();
		}
		else GD.Print("No Callback provided after White Out.");
	}
	
	public override void _Process(double delta)
	{
		if(isDoingWhiteOut) WhiteOutAnim(delta);
		if(CanMove == true && CanInteract == true)
		{
			if(@Player.Position.DistanceTo(cafeteriaDeskMarker.Position) < 5f)
			{
				canUseDesk = true;
			}
			else
			{
				canUseDesk = false;
			}
		}
		if(CanInteract == true && canUseDesk == true)
		{
			if(Input.IsActionJustReleased("action"))
			{
				bool foundMatch = false;
				if(typesHeld.Count > 0)
				{
					foreach(Recipe recipe in recipes)
					{
						foreach(IngredientType type in typesHeld)
						{
							if(typesHeld.Count < recipe.Ingredients.Count) continue;
							if(typesHeld.Contains(recipe.Ingredients[0]) &&
							typesHeld.Contains(recipe.Ingredients[1]) &&
							typesHeld.Contains(recipe.Ingredients[2]))
							{
								foundMatch = true;
								break;
							}
						}
						if(foundMatch == true) break;
					}
					if(foundMatch == true)
					{
						// Trigger teleport
						GD.Print("Teleport");
					}
				}
			}
		}
	}
	
	private void WhiteOutAnim(double delta)
	{
		whiteoutTime += (float)delta;
		float alpha = whiteoutTime / (WhiteOutAnimTime * 0.5f);
		Color start, end;
		
		if(isWhiteOut == true)
		{
			start = whiteColorTransparent;
			end = whiteColor;
		} 
		else
		{
			start = whiteColor;
			end = whiteColorTransparent;
		}
		
		WhiteOutRect.Color = start.Lerp(end, alpha);
		
		if(whiteoutTime >= WhiteOutAnimTime)
		{
			if(isWhiteOut) isWhiteOut = false;
			else
			{
				EmitSignal(SignalName.WhiteOutFinished);
				isDoingWhiteOut = false;
				isWhiteOut = true;
			}
		}
	}

	private void OnCustomerArrived(FoodItem food)
	{
		Customer customer = GetNode<Customer>("Elemental");
		Texture2D foodTexture = FoodTextures[(int)food];

		customer.RequestFood(foodTexture);
	}
	
	private void OnPickedIngredient(IngredientType type)
	{
		typesHeld.Add(type);
	}
}
