using Godot;
using System;

public partial class God : CanvasLayer
{
	public event Action OnDialogueFinished;
	
	[Export] public int MaxDialogueIndex = 1;
	[Export] public AudioStream NextDialogueSound;
	[Export] public AudioStream LastDialogueSound;
	
	public bool CanProcessInput { get; set; } = false;
	
	private int dialogueIndex = 0;
	private AudioStreamPlayer audio;
	
	public override void _Ready()
	{
		audio = GetNode<AudioStreamPlayer>("Audio");
		audio.Stream = NextDialogueSound;
	}
	
	public void EnableGod()
	{
		CanProcessInput = true;
		dialogueIndex += 1;
		NextDialogue();
	}
	
	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if(CanProcessInput)
		{
			if(Input.IsActionJustReleased("action"))
			{
				dialogueIndex += 1;
				NextDialogue();
			}
		}
	}
	
	private void NextDialogue()
	{
		if(dialogueIndex > MaxDialogueIndex)
		{
			audio.Stream = LastDialogueSound;
			audio.Play();
			OnDialogueFinished?.Invoke();
			QueueFree();
		}
		else
		{
			GetTree().CallGroup("message"+(dialogueIndex-1), Control.MethodName.Hide);
			GetTree().CallGroup("message"+dialogueIndex, Control.MethodName.Show);
			audio.Play();
		}
	}
}
