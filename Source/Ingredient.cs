using Godot;
using System;

public partial class Ingredient : RigidBody2D
{
	public Action<IngredientType> OnPickedIngredient;
	
	public const string UP = "/";
	public const string UP_RIGHT = "E";
	public const string RIGHT = ".";
	public const string DOWN_RIGHT = "W";
	public const string DOWN = ",";
	public const string DOWN_LEFT = "T";
	public const string LEFT = "Q";
	public const string UP_LEFT = "R";
	
	[Export] public ColorRect ButtonLabelBackground;
	[Export] public Label ButtonLabel;
	[Export] public Texture2D IngredientSprite;
	[Export] public IngredientType Type = IngredientType.Blue;
	
	private Player player;
	private bool isPlayerClose = false;
	
	public override void _Ready()
	{
		player = GetNode<Player>("../Player");
		if(player != null)
		{
			player.OnCollisionBegin += OnPlayerCollisionEnter;
			player.OnCollisionEnd += OnPlayerCollisionEnd;
		}
		ButtonLabelBackground.Hide();
		//GetNode<Sprite2D>("IngredientSprite").Texture = IngredientSprite;
	}
	
	public override void _Process(double delta)
	{
		if(isPlayerClose)
		{
			if(Input.IsActionJustReleased("action"))
			{
				OnPickedIngredient?.Invoke(Type);
			}
		}
	}
	
	public void SetButtonLabelText(string text) => ButtonLabel.Text = text;
	
	private void OnPlayerCollisionEnter(RigidBody2D body)
	{
		if(body == this)
		{
			SetButtonLabelText(DOWN);
			ButtonLabelBackground.Show();
		}
	}
	
	private void OnPlayerCollisionEnd(RigidBody2D body)
	{
		if(body == this) ButtonLabelBackground.Hide();
	}
	
	private void OnCollisionEnter(RigidBody2D body)
	{
		GD.Print("Enter:" + body);
		if(body == this)
		{
			SetButtonLabelText(DOWN);
			ButtonLabelBackground.Show();
			isPlayerClose = true;
		}
	}
	
	private void OnCollisionExit(RigidBody2D body)
	{
		GD.Print("Exit:" + body);
		if(body == this)
		{
			ButtonLabelBackground.Hide();
			isPlayerClose = false;
		}
	}
}
