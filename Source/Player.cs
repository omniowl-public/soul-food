using Godot;
using System;

public partial class Player : Area2D
{	
	public event Action<RigidBody2D> OnCollisionBegin;
	public event Action<RigidBody2D> OnCollisionEnd;
	
	[Export] public float Speed { get; set; } = 300f;
	
	private Main rootNode;
	
	private float rightBoundary;
	private AnimatedSprite2D animator;
	private CollisionShape2D collider;
	private Vector2 velocity;
	
	public override void _Ready()
	{
		rootNode = GetParent() as Main;
		animator = GetNode<AnimatedSprite2D>("Animator");
		collider = GetNode<CollisionShape2D>("Collider");
		Vector2 ScreenSize = GetViewportRect().Size;
		Vector2 playerSpawn = Vector2.Zero;
		if(rootNode != null)
		{
			rightBoundary = rootNode.GetNode<Node2D>("Cafeteria").GetNode<Marker2D>("RightBoundary").Position.X;
			playerSpawn = rootNode.GetNode<Node2D>("Cafeteria").GetNode<Marker2D>("PlayerSpawn").Position;
		}
		else
		{
			rightBoundary = ScreenSize.X;
			playerSpawn = new Vector2(rightBoundary * 0.5f, Position.Y);
		}
		Position = new Vector2(playerSpawn.X, Position.Y);
	}

	public override void _Process(double delta)
	{
		HandleInput(delta);
		HandleMovement();
		HandleAnimation();
	}
	
	private void HandleInput(double delta)
	{
		velocity = Vector2.Zero;
		if((rootNode != null && rootNode.CanMove) || rootNode == null)
		{
			if(Input.IsActionPressed("move_left")) velocity += Vector2.Left;
			if(Input.IsActionPressed("move_right")) velocity += Vector2.Right;
			velocity = velocity.Normalized() * Speed * (float)delta;
		}
		
		if((rootNode != null && rootNode.CanInteract) || rootNode == null)
		{
			
		}
	}
	
	private void HandleMovement()
	{
		if((rootNode != null && rootNode.CanMove) || rootNode == null)
		{
			Position += velocity;
			float x = Mathf.Clamp(Position.X, 0, rightBoundary);
			Position = new Vector2(x, Position.Y);
		}
	}
	
	private void HandleAnimation()
	{
		if(velocity.Length() > 0) 
		{
			animator.Play(name: "walk");
			animator.FlipH = velocity.X < 0;
		}
		else animator.Play(name:"idle");
	}
	
	private void OnBodyEntered(Node2D body) => OnCollisionBegin?.Invoke(body as RigidBody2D);
	private void OnBodyExited(Node2D body) => OnCollisionEnd?.Invoke(body as RigidBody2D);
}
